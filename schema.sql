--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: education; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.education (
    sno integer NOT NULL,
    edu_id character varying(255),
    name character varying(255),
    type character varying(255),
    id character varying(255),
    linkedin_url character varying(255),
    facebook_url character varying(255),
    twitter_url character varying(255),
    linkedin_id character varying(255),
    website character varying(255),
    domain character varying(255),
    location character varying(255),
    degrees character varying(255),
    majors character varying(255),
    minors character varying(255),
    start_date character varying(255),
    "end-date" character varying(255),
    gpa character varying(255),
    summary character varying(4000)
);


ALTER TABLE public.education OWNER TO postgres;

--
-- Name: education_sno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.education ALTER COLUMN sno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.education_sno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100
    CACHE 1
);


--
-- Name: emails; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.emails (
    sno integer NOT NULL,
    mailid character varying(255),
    address character varying(255),
    type character varying(255)
);


ALTER TABLE public.emails OWNER TO postgres;

--
-- Name: emails_sno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.emails ALTER COLUMN sno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.emails_sno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100
    CACHE 1
);


--
-- Name: experience; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.experience (
    sno integer NOT NULL,
    exp_id character varying(255),
    name character varying(255),
    size character varying(255),
    id character varying(255),
    founded character varying(255),
    industry character varying(255),
    lname character varying(255),
    locality character varying(255),
    region character varying(255),
    country character varying(255),
    continent character varying(255),
    street_address character varying(255),
    address_line_2 character varying(255),
    postal_code character varying(255),
    geo character varying(255),
    linkedin_url character varying(255),
    linkedin_id character varying(255),
    facebook_url character varying(255),
    twitter_url character varying(255),
    website character varying(255),
    location_name character varying(255),
    end_date character varying(255),
    start_date character varying(255),
    title_name character varying(255),
    title_role character varying(255),
    title_level character varying(255),
    is_primary character varying(255),
    summary character varying(4000)
);


ALTER TABLE public.experience OWNER TO postgres;

--
-- Name: experience_sno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.experience ALTER COLUMN sno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.experience_sno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100
    CACHE 1
);


--
-- Name: main1; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main1 (
    sno integer NOT NULL,
    job_company_location_geo character varying(255),
    job_company_name character varying(255),
    birth_date character varying(255),
    middle_intial character varying(255),
    job_company_size character varying(255),
    linkedin_username character varying(255),
    birth_year character varying(255),
    job_company_location_locality character varying(255),
    id character varying(255),
    job_title character varying(255),
    facebook_url character varying(255),
    job_company_id character varying(255),
    location_region character varying(255),
    location_street_address character varying(255),
    location_address_line_2 character varying(255),
    job_company_location_postal_code character varying(255),
    facebook_id character varying(255),
    location_name character varying(255),
    full_name character varying(255),
    location_last_updated character varying(255),
    version_contains character varying(255),
    previous_version character varying(255),
    current_version character varying(255),
    version_status character varying(255),
    job_company_location_street_address character varying(255),
    job_company_founded character varying(255),
    linkedin_url character varying(255),
    work_email character varying(255),
    job_company_location_region character varying(255),
    job_company_website character varying(255),
    gender character varying(255),
    location_country character varying(255),
    facebook_username character varying(255),
    twitter_username character varying(255),
    job_company_location_country character varying(255),
    github_username character varying(255),
    industry character varying(255),
    location_geo character varying(255),
    job_company_location_address_line_2 character varying(255),
    location_postal_code character varying(255),
    mobile_phone character varying(255),
    location_continent character varying(255),
    location_locality character varying(255),
    first_name character varying(255),
    twitter_url character varying(255),
    job_company_location_continent character varying(255),
    job_company_facebook_url character varying(255),
    job_company_linkedin_url character varying(255),
    last_name character varying(255),
    linkedin_id character varying(255),
    job_last_updated character varying(255),
    middle_name character varying(255),
    job_company_twitter_url character varying(255),
    job_title_role character varying(255),
    job_company_linkedin_id character varying(255),
    github_url character varying(255),
    job_company_industry character varying(255),
    job_company_location_name character varying(255),
    skills character varying(255),
    job_title_levels character varying(255),
    countries character varying(255),
    location_names character varying(255),
    regions character varying(255),
    street_addresses character varying(255),
    phone_numbers character varying(255),
    interests character varying(255),
    inferred_salary character varying(255),
    inferred_years_experience character varying(255),
    summary character varying(255)
);


ALTER TABLE public.main1 OWNER TO postgres;

--
-- Name: main1_sno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.main1 ALTER COLUMN sno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.main1_sno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100
    CACHE 1
);


--
-- Name: profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profile (
    sno integer NOT NULL,
    pro_id character varying(255),
    network character varying(255),
    id character varying(255),
    url character varying(255),
    user_name character varying(255)
);


ALTER TABLE public.profile OWNER TO postgres;

--
-- Name: profile_sno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.profile ALTER COLUMN sno ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.profile_sno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100
    CACHE 1
);


--
-- Name: education edu_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.education
    ADD CONSTRAINT edu_id UNIQUE (edu_id);


--
-- Name: education education_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.education
    ADD CONSTRAINT education_pkey PRIMARY KEY (sno);


--
-- Name: emails emails_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emails
    ADD CONSTRAINT emails_pkey PRIMARY KEY (sno);


--
-- Name: experience exp_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experience
    ADD CONSTRAINT exp_id UNIQUE (exp_id);


--
-- Name: profile id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT id UNIQUE (id);


--
-- Name: main1 id_uk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main1
    ADD CONSTRAINT id_uk UNIQUE (id);


--
-- Name: emails mailid_u; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emails
    ADD CONSTRAINT mailid_u UNIQUE (mailid);


--
-- Name: main1 main1_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main1
    ADD CONSTRAINT main1_pkey PRIMARY KEY (sno);


--
-- Name: profile pro_id_u; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT pro_id_u UNIQUE (pro_id);


--
-- Name: profile profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (sno);


--
-- Name: education edu_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.education
    ADD CONSTRAINT edu_id_fk FOREIGN KEY (edu_id) REFERENCES public.profile(id) NOT VALID;


--
-- Name: experience exp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experience
    ADD CONSTRAINT exp_id_fk FOREIGN KEY (exp_id) REFERENCES public.profile(id) NOT VALID;


--
-- Name: main1 id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main1
    ADD CONSTRAINT id_fk FOREIGN KEY (id) REFERENCES public.profile(id) NOT VALID;


--
-- Name: profile pro_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT pro_id_fk FOREIGN KEY (pro_id) REFERENCES public.emails(mailid) NOT VALID;


--
-- PostgreSQL database dump complete
--

