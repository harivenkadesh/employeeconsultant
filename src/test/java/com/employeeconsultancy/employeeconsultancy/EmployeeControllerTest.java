package com.employeeconsultancy.employeeconsultancy;

import com.employeeconsultancy.employeeconsultancy.entity.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import java.time.OffsetDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@AutoConfigureMockMvc
@DisplayName("Employee test")
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Create profile")
    void test1B() throws Exception {
        var profile = new ProfileInfo("Test@gmail.com", "test_network", "testId", "testUrl", "Test_user");
        addProfile(profile, status().isOk());
    }

    private ResultActions addProfile(ProfileInfo profile, ResultMatcher status) throws Exception {
        return mockMvc.perform(post("/employees/profile")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(profile)))
                .andDo(print())
                .andExpect(status);
    }

    @Test
    @DisplayName("create Education")
    void test1C() throws Exception {
        var education = new EducationInfo("testId", "Test_name", "test_type", "testId", "test_linkedInUrl", "test_facebookUrl",
                "test_twitterUrl", "test_LinkedinId", "test_website", "test_domain", "test_location", "test_degree", "test_majors",
                "test_minors", "test_startDate", "test_endDate", "test_gpa", "summary");
        addEducation(education, status().isOk());
    }

    private ResultActions addEducation(EducationInfo educationInfo, ResultMatcher status) throws Exception {
        return mockMvc.perform(post("/employee/education")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(educationInfo)))
                .andDo(print())
                .andExpect(status);
    }

    @Test
    @DisplayName("create email")
    void test1A() throws Exception {
        var email = new EmailsInfo("Test@gmail.com", "test_address", "test_type");
        addEmail(email, status().isOk());
    }

    private ResultActions addEmail(EmailsInfo emailsInfo, ResultMatcher status) throws Exception {
        return mockMvc.perform(post("/employee/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailsInfo)))
                .andDo(print())
                .andExpect(status);
    }

    @Test
    @DisplayName("create experience")
    void test1D() throws Exception {
        var experience = new ExperienceInfo("testId", "test_name", "test_size", "testId", "test_founded", "test_industry",
                "test_lname", "test_locality", "test_region", "test_country", "test_continent", "test_streetAddress", "test_addressLine_2",
                "test_poatalcode", "test_geo", "test_lnkedinUrl", "test_linkedinId", "test_faceBookUrl", "test_twitterUrl", "test_website",
                "test_LocationName", OffsetDateTime.now().plusDays(3).toString(), OffsetDateTime.now().toString(), "test_titleName",
                "test_titleRole", "test_titleLevel", "test_isPrimary", "test_summary");

        addExperience(experience, status().isOk());
    }

    private ResultActions addExperience(ExperienceInfo experienceInfo, ResultMatcher status) throws Exception {
        return mockMvc.perform(post("/employee/experience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(experienceInfo)))
                .andDo(print())
                .andExpect(status);
    }

    @Test
    @DisplayName("create Main")
    void test1E() throws Exception {
        var main = new MainInfo("jobCompanyLocationGeo", "jobCompanyName", "test_birthDate", "middleIntial", "jobCompanySize",
                "LinkedinUsername", "birthyear", "jobCompanyLocationLocality", "testId", "jobTitle", "facebookUrl", "jobCompanyId",
                "locationRegion", "locationStreetAdress", "locationAddressLine2", "jobCompanyLocationPostalCode", "facebookId",
                "locationName", "fullname", "locationLastUpdated", "versionContains", "previousVersion", "currentVersion", "versionStatus",
                "jobCompanyLocationStreetAddress", "jobCompanyFounded", "linkedinUrl", "workEmail", "jobCompanyLocationRegion",
                "jobCompanyWebsite", "male", "locationCountry", "facebookUsername", "twitterUsername", "jobCompanyLocationountry",
                "githubUsername", "industry", "locationGeo", "jobCompanyLocationAddressLine2", "locationPostalCode",
                "9999999999", "locationContinent", "locationLocality", "Test_name", "twitterUrl", "jobCompanyLocationContinent",
                "jobCompanyFacebookUrl", "jobCompanyLinkedinUrl", "test_lastName", "linkedinId", "JoblastUpdate", "middleName",
                "jobCompanyTwitterUrl", "jobTitleRole", "jobCompanyLinkedinId", "githubUrl", "jobCompanyIndustry", "jobCompanyLocationName",
                "skills", "jobtitleLevels", "Countries", "locationNames", "regions", "streetAddresses", "0422-222333", "interest",
                "inferredSalary", "inferredYearsExperience", "test_Summary");
        addMain(main, status().isOk());
    }

    private ResultActions addMain(MainInfo mainInfo, ResultMatcher status) throws Exception {
        return mockMvc.perform(post("/employee/main")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mainInfo)))
                .andDo(print())
                .andExpect(status);
    }

    @Test
    @DisplayName("TearDown")
    public void test1Y() throws Exception {
        mockMvc.perform(delete("/employee"))
                .andDo(print());
    }
}
