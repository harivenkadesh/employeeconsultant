package com.employeeconsultancy.employeeconsultancy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class ProfileInfo {
    private String proId;
    private String network;
    private String id;
    private String url;
    private String userName;
}
