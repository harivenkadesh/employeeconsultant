package com.employeeconsultancy.employeeconsultancy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class ExperienceInfo {
    private String expId;
    private String name;
    private String size;
    private String id;
    private String founded;
    private String industry;
    private String lName;
    private String locality;
    private String region;
    private String country;
    private String continent;
    private String streetAddress;
    private String addressLine_2;
    private String postalCode;
    private String geo;
    private String linkedinUrl;
    private String linkedinId;
    private String facebookUrl;
    private String twitterUrl;
    private String website;
    private String locationName;
    private String endDate;
    private String startDate;
    private String titleName;
    private String titleRole;
    private String titleLevel;
    private String isPrimary;
    private String summary;
}
