package com.employeeconsultancy.employeeconsultancy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class EducationInfo {
    private String eduId;
    private String name;
    private String type;
    private String id;
    private String linkedinUrl;
    private String facebookUrl;
    private String twitterUrl;
    private String linkedinId;
    private String website;
    private String domain;
    private String location;
    private String degrees;
    private String majors;
    private String minors;
    private String startDate;
    private String endDate;
    private String gpa;
    private String summary;
}
