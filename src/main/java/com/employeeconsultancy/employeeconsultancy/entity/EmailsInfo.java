package com.employeeconsultancy.employeeconsultancy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class EmailsInfo {
    private String mail_Id;
    private String address;
    private String type;
}
