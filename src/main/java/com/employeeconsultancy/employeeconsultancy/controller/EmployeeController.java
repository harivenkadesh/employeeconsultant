package com.employeeconsultancy.employeeconsultancy.controller;

import com.employeeconsultancy.employeeconsultancy.entity.*;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Education;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Emails;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Main1;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Profile;
import com.employeeconsultancy.employeeconsultancy.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeRepository employeeRepository;

    @PostMapping("/employees/profile")
    public ResponseEntity<Profile> createProfile(@RequestBody ProfileInfo profileInfo) {
        return ResponseEntity.ok(employeeRepository.addProfile(profileInfo));
    }

    @PostMapping("/employee/education")
    public ResponseEntity<Education> createEducation(@RequestBody EducationInfo educationInfo) {
        return ResponseEntity.ok(employeeRepository.addEducation(educationInfo));
    }

    @PostMapping("/employee/email")
    public ResponseEntity<Emails> createEmail(@RequestBody EmailsInfo emailsInfo) {
        return ResponseEntity.ok(employeeRepository.addEmails(emailsInfo));
    }

    @PostMapping("/employee/experience")
    public ResponseEntity<?> createExperience(@RequestBody ExperienceInfo experienceInfo) {
        employeeRepository.addExperience(experienceInfo);
        return new ResponseEntity<>("Experience Added Successfully", HttpStatus.OK);
    }

    @PostMapping("/employee/main")
    public ResponseEntity<Main1> createMain(@RequestBody MainInfo mainInfo) {
        return ResponseEntity.ok(employeeRepository.addMain(mainInfo));
    }
    @DeleteMapping("/employee")
    public void deleteAll(){
        employeeRepository.clearAll();
    }
}
