package com.employeeconsultancy.employeeconsultancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeconsultancyApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeconsultancyApplication.class, args);
	}

}
