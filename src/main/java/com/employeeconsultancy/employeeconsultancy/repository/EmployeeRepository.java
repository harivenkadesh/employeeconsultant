package com.employeeconsultancy.employeeconsultancy.repository;

import com.employeeconsultancy.employeeconsultancy.entity.*;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Education;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Emails;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Main1;
import com.employeeconsultancy.employeeconsultancy.model.tables.pojos.Profile;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import static com.employeeconsultancy.employeeconsultancy.model.Tables.*;

@Repository
@RequiredArgsConstructor
public class EmployeeRepository {
    private final DSLContext db;

    public Profile addProfile(ProfileInfo profileInfo) {
        var profileRecord = db.newRecord(PROFILE, new Profile(null, profileInfo.getProId(), profileInfo.getNetwork(),
                profileInfo.getId(), profileInfo.getUrl(), profileInfo.getUserName()));
        profileRecord.store();
        return profileRecord.into(Profile.class);
    }

    public Education addEducation(EducationInfo educationInfo) {
        var educationRecord = db.newRecord(EDUCATION, new Education(null, educationInfo.getEduId(),
                educationInfo.getName(), educationInfo.getType(), educationInfo.getId(), educationInfo.getLinkedinUrl(),
                educationInfo.getFacebookUrl(), educationInfo.getTwitterUrl(), educationInfo.getLinkedinId(),
                educationInfo.getWebsite(), educationInfo.getDomain(), educationInfo.getLocation(), educationInfo.getDegrees(),
                educationInfo.getMajors(), educationInfo.getMinors(), educationInfo.getStartDate(), educationInfo.getEndDate(),
                educationInfo.getGpa(), educationInfo.getSummary()));
        educationRecord.store();
        return educationRecord.into(Education.class);
    }

    public Emails addEmails(EmailsInfo emailsInfo) {
        var emailRecord = db.newRecord(EMAILS, new Emails(null, emailsInfo.getMail_Id(), emailsInfo.getAddress(),
                emailsInfo.getType()));
        emailRecord.store();
        return emailRecord.into(Emails.class);
    }

    public void addExperience(ExperienceInfo experienceInfo) {
        db.insertInto(EXPERIENCE, EXPERIENCE.EXP_ID, EXPERIENCE.NAME, EXPERIENCE.SIZE, EXPERIENCE.ID,
                EXPERIENCE.FOUNDED, EXPERIENCE.INDUSTRY, EXPERIENCE.LNAME, EXPERIENCE.LOCALITY, EXPERIENCE.REGION, EXPERIENCE.COUNTRY,
                EXPERIENCE.CONTINENT, EXPERIENCE.STREET_ADDRESS, EXPERIENCE.ADDRESS_LINE_2, EXPERIENCE.POSTAL_CODE, EXPERIENCE.GEO,
                EXPERIENCE.LINKEDIN_URL, EXPERIENCE.LINKEDIN_ID, EXPERIENCE.FACEBOOK_URL, EXPERIENCE.TWITTER_URL, EXPERIENCE.WEBSITE,
                EXPERIENCE.LOCATION_NAME, EXPERIENCE.END_DATE, EXPERIENCE.START_DATE, EXPERIENCE.TITLE_NAME, EXPERIENCE.TITLE_ROLE,
                EXPERIENCE.TITLE_LEVEL, EXPERIENCE.IS_PRIMARY, EXPERIENCE.SUMMARY)
                .values(experienceInfo.getExpId(), experienceInfo.getName(), experienceInfo.getSize(),
                        experienceInfo.getId(), experienceInfo.getFounded(), experienceInfo.getIndustry(), experienceInfo.getLName(), experienceInfo.getLocality(),
                        experienceInfo.getRegion(), experienceInfo.getCountry(), experienceInfo.getContinent(), experienceInfo.getStreetAddress(), experienceInfo.getAddressLine_2(),
                        experienceInfo.getPostalCode(), experienceInfo.getGeo(), experienceInfo.getLinkedinUrl(), experienceInfo.getLinkedinId(), experienceInfo.getFacebookUrl(),
                        experienceInfo.getTwitterUrl(), experienceInfo.getWebsite(), experienceInfo.getLocationName(), experienceInfo.getEndDate(), experienceInfo.getStartDate(),
                        experienceInfo.getTitleName(), experienceInfo.getTitleRole(), experienceInfo.getTitleLevel(), experienceInfo.getIsPrimary(),
                        experienceInfo.getSummary()).execute();
    }

    public Main1 addMain(MainInfo mainInfo) {
        var mainRecord = db.newRecord(MAIN1, new Main1(null, mainInfo.getJobCompanyLocationGeo(), mainInfo.getJobCompanyName(), mainInfo.getBirthDate(),
                mainInfo.getMiddleIntial(), mainInfo.getJobCompanySize(), mainInfo.getLinkedinUsername(), mainInfo.getBirthYear(), mainInfo.getJobCompanyLocationLocality(),
                mainInfo.getId(), mainInfo.getJobTitle(), mainInfo.getFacebookUrl(), mainInfo.getJobCompanyId(), mainInfo.getLocationRegion(), mainInfo.getLocationStreetAddress(),
                mainInfo.getLocationAddressLine_2(), mainInfo.getJobCompanyLocationPostalCode(), mainInfo.getFacebookId(), mainInfo.getLocationName(), mainInfo.getFullName(),
                mainInfo.getLocationLastUpdated(), mainInfo.getVersionContains(), mainInfo.getPreviousVersion(), mainInfo.getCurrentVersion(), mainInfo.getVersionStatus(),
                mainInfo.getJobCompanyLocationStreetAddress(), mainInfo.getJobCompanyFounded(), mainInfo.getLinkedinUrl(), mainInfo.getWorkEmail(), mainInfo.getJobCompanyLocationRegion(),
                mainInfo.getJobCompanyWebsite(), mainInfo.getGender(), mainInfo.getLocationCountry(), mainInfo.getFacebookUsername(), mainInfo.getTwitterUsername(),
                mainInfo.getJobCompanyLocationCountry(), mainInfo.getGithubUsername(), mainInfo.getIndustry(), mainInfo.getLocationGeo(), mainInfo.getJobCompanyLocationAddressLine_2(),
                mainInfo.getLocationPostalCode(), mainInfo.getMobilePhone(), mainInfo.getLocationContinent(), mainInfo.getLocationLocality(), mainInfo.getFirstName(),
                mainInfo.getTwitterUrl(), mainInfo.getJobCompanyLocationContinent(), mainInfo.getJobCompanyFacebookUrl(), mainInfo.getJobCompanyLinkedinUrl(),
                mainInfo.getLastName(), mainInfo.getLinkedinId(), mainInfo.getJobLastUpdated(), mainInfo.getMiddleName(), mainInfo.getJobCompanyTwitterUrl(), mainInfo.getJobTitleRole(),
                mainInfo.getJobCompanyLinkedinId(), mainInfo.getGithubUrl(), mainInfo.getJobCompanyIndustry(), mainInfo.getJobCompanyLocationName(), mainInfo.getSkills(),
                mainInfo.getJobTitleLevels(), mainInfo.getCountries(), mainInfo.getLocationNames(), mainInfo.getRegions(), mainInfo.getStreetAddresses(), mainInfo.getPhoneNumbers(),
                mainInfo.getInterests(), mainInfo.getInferredSalary(), mainInfo.getInferredYearsExperience(), mainInfo.getSummary()));
        mainRecord.store();
        return mainRecord.into(Main1.class);
    }

    public void clearAll() {
        db.batch(db.delete(EXPERIENCE),db.delete(EDUCATION),db.delete(MAIN1),db.delete(PROFILE),db.delete(EMAILS))
                .execute();
    }
}
